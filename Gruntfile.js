/**/

module.exports = function (grunt) {

    require('jit-grunt')(grunt);

    grunt.initConfig({

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'app/src/scss/app.css': ['app/src/scss/main.scss']
                }
            }
        },

        copy: {
            dist: {
                files: [{
                    expand: true,
                    src: ['app/src/*'],
                    dest: 'app/dist/',
                    filter: 'isFile',
                    flatten: 'true'
                }]
            }
        },

        cssmin: {
            dist: {
                files: {
                    'app/dist/app.min.css': 'app/src/scss/app.css'
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    'app/dist/app.min.js': 'app/src/js/app.js'
                }
            }
        }
    });

    grunt.registerTask('default', ['sass', 'cssmin', 'uglify', 'copy']);

};